import cv2
import numpy as np

from datasets.dataset.jde import letterbox

class ImageLoader:  # for inum_imageserence
    """This allows to iterate over images from on sequence"""
    def __init__(self, images, img_size=(1088, 608)):
        self.images = images

        self.num_images = len(self.images)  # number of image files
        self.width = img_size[0]
        self.height = img_size[1]
        self.count = 0

        assert self.num_images > 0, 'No images found'

    def __iter__(self):
        self.count = -1
        return self

    def __next__(self):
        self.count += 1
        if self.count == self.num_images:
            raise StopIteration
        img0 = self.images[self.count]

        # Padded resize
        img, _, _, _ = letterbox(img0, height=self.height, width=self.width)

        # Normalize RGB
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img, dtype=np.float32)
        img /= 255.0

        # cv2.imwrite(img_path + '.letterbox.jpg', 255 * img.transpose((1, 2, 0))[:, :, ::-1])  # save letterbox image
        return "", img, img0

    def __getitem__(self, idx):
        idx = idx % self.num_images
        img0 = self.images[idx]

        # Padded resize
        img, _, _, _ = letterbox(img0, height=self.height, width=self.width)

        # Normalize RGB
        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img, dtype=np.float32)
        img /= 255.0

        return "", img, img0

    def __len__(self):
        return self.num_images  # number of files