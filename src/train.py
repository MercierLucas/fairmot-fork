from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import _init_paths

import os
import json
import wandb
import torch
import torch.utils.data
from datetime import datetime
from torchvision.transforms import transforms as T
from opts import opts
from fairmot_models.model import create_model, load_model, save_model
from fairmot_models.data_parallel import DataParallel
from logger import Logger
from datasets.dataset_factory import get_dataset
from trains.train_factory import train_factory
import motmetrics as mm
from fairmot_models.decode import mot_decode
from fairmot_metrics import evaluate_sequence
from detect import merge_outputs, get_meta, post_process
import datasets.dataset.jde as datasets

from tracking_utils.utils import mkdir_if_missing, tlbr2tlwh
from track import eval_seq
from tracking_utils.evaluation import Evaluator

from tracking_utils.io import read_results


class FairmotTrainer:
    """ Wrapper for Fairmot training """
    def __init__(self, opt):
        """Main method for training

        Args:
            opt: Arguments parsed
        """

        self.opt = opt
        torch.manual_seed(opt.seed)
        torch.backends.cudnn.benchmark = not opt.not_cuda_benchmark and not opt.test

        print('Setting up data...')
        Dataset = get_dataset(opt.dataset, opt.task)
        f = open(opt.data_cfg)
        data_config = json.load(f)
        trainset_paths = data_config['train']
        valset_paths = data_config['test']

        dataset_root = data_config['root']
        f.close()
        transforms = T.Compose([T.ToTensor()])
        dataset = Dataset(opt, dataset_root, trainset_paths, (1088, 608), augment=True, transforms=transforms)
        val_dataset = Dataset(opt, dataset_root, valset_paths, (1088, 608), augment=True, transforms=transforms)
        #opt = opts().update_dataset_info_and_set_heads(opt, dataset)
        print(opt)

        self.logger = Logger(opt)

        os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
        opt.device = torch.device('cuda' if opt.gpus[0] >= 0 else 'cpu')

        print('Creating model...')
        self.model = create_model(opt.arch, opt.heads, opt.head_conv)
        self.optimizer = torch.optim.Adam(self.model.parameters(), opt.lr)
        self.start_epoch = 0

        # Get dataloader
        self.train_loader = torch.utils.data.DataLoader(
            dataset,
            batch_size=opt.batch_size,
            shuffle=True,
            num_workers=opt.num_workers,
            pin_memory=True,
            drop_last=True
        )

        self.val_loader = torch.utils.data.DataLoader(
            val_dataset,
            batch_size=opt.batch_size,
            shuffle=True,
            num_workers=opt.num_workers,
            pin_memory=True,
            drop_last=True
        )

        print('Starting training...')
        Trainer = train_factory[opt.task]
        self.trainer = Trainer(opt, self.model, self.optimizer)
        self.trainer.set_device(opt.gpus, opt.chunk_sizes, opt.device)

        if opt.load_model != '':
            self.model, self.optimizer, self.start_epoch = load_model(
                self.model, opt.load_model, self.trainer.optimizer, opt.resume, opt.lr, opt.lr_step)

        self.epoch = 0
        self.mark = 0 if opt.save_all else 'last'


    def save(self, mark=None):
        """Save current model"""
        if not mark:
            mark = self.mark

        save_model(os.path.join(self.opt.save_dir, 'model_{}.pth'.format(mark)),self.epoch, self.model, self.optimizer)


    def train(self):
        """Main method for training

        Args:
            opt: Arguments parsed
        """
        for self.epoch in range(self.start_epoch + 1, self.opt.num_epochs + 1):
            self.mark = self.epoch if self.opt.save_all else 'last'
            log_dict_train, _ = self.trainer.train(self.epoch, self.train_loader)
            self.logger.write('epoch: {} |'.format(self.epoch))
            self.log_losses(log_dict_train,"train", self.epoch)

            if self.opt.val_intervals > 0 and self.epoch % self.opt.val_intervals == 0:
                self.save()
            else:
                self.save(mark = 'last')

            self.logger.write('\n')

            if self.epoch in self.opt.lr_step:
                self.save()
                lr = self.opt.lr * (0.1 ** (self.opt.lr_step.index(self.epoch) + 1))
                print('Drop LR to', lr)
                for param_group in self.optimizer.param_groups:
                    param_group['lr'] = lr
            if self.epoch % 5 == 0 or self.epoch >= 25:
                self.save(mark = self.epoch)

            log_dict_val, _ = self.trainer.val(self.epoch, self.val_loader)
            self.log_losses(log_dict_val, "val", self.epoch)

            # if log_dict_val["loss"] < opt.best_value:
            #     print(f"New best value with {log_dict_val['loss']:.2f} saved")
            #     save_model(os.path.join(opt.save_dir,'fairmot_best.pth'),
            #             self.epoch, self.model, self.optimizer)

            res = {'loss':log_dict_train['loss'],'val_loss':log_dict_val['loss']}
            yield res

        self.logger.close()

    def train(self):
        """Main method for training

        Args:
            opt: Arguments parsed
        """
        best_loss = 1e15
        for self.epoch in range(self.start_epoch + 1, self.opt.num_epochs + 1):
            self.mark = self.epoch if self.opt.save_all else 'last'
            log_dict_train, _ = self.trainer.train(self.epoch, self.train_loader)
            self.logger.write('epoch: {} |'.format(self.epoch))
            self.log_losses(log_dict_train, "train", self.epoch)

            if self.opt.val_intervals > 0 and self.epoch % self.opt.val_intervals == 0:
                self.save()
            else:
                self.save(mark = 'last')

            self.logger.write('\n')

            if self.epoch in self.opt.lr_step:
                self.save()
                lr = self.opt.lr * (0.1 ** (self.opt.lr_step.index(self.epoch) + 1))
                print('Drop LR to', lr)
                for param_group in self.optimizer.param_groups:
                    param_group['lr'] = lr
            if self.epoch % 5 == 0 or self.epoch >= 25:
                self.save(mark = self.epoch)

            log_dict_val, _ = self.trainer.val(self.epoch, self.val_loader)
            self.log_losses(log_dict_val, "val", self.epoch)


            val_hm_loss = log_dict_val['hm_loss']           
            val_wh_loss = log_dict_val['wh_loss']
            val_off_loss = log_dict_val['off_loss'] 
            val_loss_no_id = val_hm_loss + val_off_loss + 0.1 * val_wh_loss
            
            id_loss = log_dict_train['id_loss']
            hm_loss = log_dict_train['hm_loss']
            wh_loss = log_dict_train['wh_loss']
            off_loss = log_dict_train['off_loss']
            loss_no_id = hm_loss + off_loss + 0.1 * val_wh_loss
            res = {
                'loss_id' : log_dict_train['loss'],
                'loss_no_id' : loss_no_id,
                'hm_loss' : hm_loss,
                'wh_loss' : wh_loss,
                'off_loss' : off_loss,
                'id_loss' : id_loss,
                'val_loss_no_id': val_loss_no_id,
                'hm_val_loss' : val_hm_loss,
                'wh_val_loss' : val_wh_loss,
                'off_val_loss' : val_off_loss,
            }

            if val_loss_no_id < best_loss:
                print(f"New best value with {val_loss_no_id:.2f} saved")
                save_model(os.path.join(opt.save_dir, f'fairmot_{val_loss_no_id}.pth'),
                        self.epoch, self.model, self.optimizer)
                best_loss = val_loss_no_id

            yield res
        self.logger.close()


    def log_losses(self, loss, dataset, epoch):
        """Write computed losses infos into the logger
        
        Args:
            logger: An instance of the Logger class
            loss: A dict that contains losses such as losse's name is the key and the result is the value
            dataset: wether it's from the train or val
            epoch: the current epoch for which losses has been computed
        """
        assert dataset in ["train","val"], "Origin dataset must be either 'train' or 'val'"

        for k, v in loss.items():
            self.logger.scalar_summary(f'{dataset}_{k}', v, epoch)
            self.logger.write('{} {:8f} | '.format(k, v))


if __name__ == '__main__':
    os.environ['CUDA_VISIBLE_DEVICES'] = '0, 1'
    opt = opts().init()

    now = datetime.now()
    date_time = now.strftime('%m_%d_%Y_%H_%M_%S')

    wandb.login()
    wandb.init(
        project=opt.exp_id,
        name=date_time,
        config=opt
    )

    trainer = FairmotTrainer(opt)
    for res in trainer.train():
        wandb.log(res)
